import 'package:http/http.dart';
import 'dart:convert';


class ReStatistic {

  String statistic_id;
  String statistic_hits; 
  String statistic_fails; 
  String user_id; 
  Map statistic_known; 
  int knownWords = 0;
  int word_id;
  num attempts = 0;
  bool whatToDo = false; // false - add word with parameters true - update parameters
  int wordHits;
  int wordFails;
  int wordAttempts;
  bool wordKnown;

  String done;


  ReStatistic({ this.user_id, this.word_id, this.done });


  Future<void> sendData() async {

    try {
      //make the request
      Response response = await post('http://serwer1969421.home.pl/wkuwacz/statistics.php', body: {
      "user_id" : user_id
      });
      List data = jsonDecode(response.body);
      Map dataMap = data[0];
      statistic_id = dataMap['statistic_id'];
      user_id = dataMap['statistic_user_id'];
      statistic_hits = dataMap['statistic_hits'];
      statistic_fails = dataMap['statistic_fails'];

      statistic_known = jsonDecode(dataMap['statistic_known']);
      List wordsList = statistic_known['words'];
      int tmpVal;

      for (var i = 0; i < statistic_known['words'].length; i++){
        if(wordsList[i]['id'] == word_id){
          whatToDo = true;
          if(done == 'true'){
            tmpVal = int.parse(statistic_hits) + 1;
            statistic_hits = tmpVal.toString();
            wordAttempts = wordsList[i]['attempts'] + 1;
            wordHits = wordsList[i]['hits'] + 1;
            wordFails = wordsList[i]['fails'];
            if(wordHits >= 5){
              wordKnown = true ;
            }else{
              wordKnown = false;
            }
            wordsList.removeWhere((item) => item['id'] == word_id);
            Map<String, dynamic> newWord = {
            "id": word_id,
            "attempts": wordAttempts,
            "hits": wordHits,
            "fails": wordFails,
            "known": wordKnown
            };
            wordsList.add(newWord);

          }else{
            tmpVal = int.parse(statistic_fails) + 1;
            statistic_hits = tmpVal.toString();
            wordAttempts = wordsList[i]['attempts'] + 1;
            wordHits = wordsList[i]['hits'];
            wordFails = wordsList[i]['fails'] + 1;
            if(wordHits >= 5){
              wordKnown = true ;
            }else{
              wordKnown = false;
            }
            wordsList.removeWhere((item) => item['id'] == word_id);
            Map<String, dynamic> newWord = {
            "id": word_id,
            "attempts": wordAttempts,
            "hits": wordHits,
            "fails": wordFails,
            "known": wordKnown
            };
            wordsList.add(newWord);
          }
          break;
        }

        
      }

        if(whatToDo == false){
          if(done == 'true'){
            tmpVal = int.parse(statistic_hits) + 1;
            statistic_hits = tmpVal.toString();
            wordAttempts = 1;
            wordHits = 1;
            wordFails = 0;
            if(wordHits >= 5){
              wordKnown = true ;
            }else{
              wordKnown = false;
            }
            Map<String, dynamic> newWord = {
            "id": word_id,
            "attempts": wordAttempts,
            "hits": wordHits,
            "fails": wordFails,
            "known": wordKnown
            };
            wordsList.add(newWord);

          }
          else{
            tmpVal = int.parse(statistic_fails) + 1;
            statistic_fails = tmpVal.toString();
            wordAttempts = 1;
            wordHits = 0;
            wordFails = 1;
            if(wordHits >= 5){
              wordKnown = true ;
            }
            else{
              wordKnown = false;
            }
            Map<String, dynamic> newWord = {
            "id": word_id,
            "attempts": wordAttempts,
            "hits": wordHits,
            "fails": wordFails,
            "known": wordKnown
            };
            wordsList.add(newWord);
          }
        };

        
        Map newMap = {"words" : wordsList };

      print('user_id: ' + user_id);
      print('hits: ' +statistic_hits);
      print('fails: ' + statistic_fails);

      Response dataToSerwer = await post('http://serwer1969421.home.pl/wkuwacz/update_statistics.php', body: {
      "user_id" : user_id,
      "statistic_hits" : statistic_hits,
      "statistic_fails" : statistic_fails,
      "wordsList" : jsonEncode(newMap)
      });
    }
    catch (e) {
      print('caught error: $e');
      statistic_id = '0';
      user_id = '0';
      statistic_hits = '0';
      statistic_fails = '0';
      statistic_known = {"result": "error"};
    }


  }

}
