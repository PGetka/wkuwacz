import 'package:http/http.dart';
import 'dart:convert';

class GetStatistic {

  String statistic_id;
  String statistic_hits; 
  String statistic_fails; 
  String user_id; 
  Map statistic_known; 
  int knownWords = 0;
  num attempts = 0;
  num avgAns;


  GetStatistic({ this.user_id });


  Future<void> checkData() async {

    try {

      //make the request
      Response response = await post('http://serwer1969421.home.pl/wkuwacz/statistics.php', body: {
      "user_id" : user_id,
      });
      List data = jsonDecode(response.body);
      Map dataMap = data[0];

      statistic_id = dataMap['statistic_id'];
      user_id = dataMap['statistic_user_id'];
      statistic_hits = dataMap['statistic_hits'];
      statistic_fails = dataMap['statistic_fails'];
      statistic_known = jsonDecode(dataMap['statistic_known']);
      List wordsList = statistic_known['words'];

      for (var i = 0; i < statistic_known['words'].length; i++){
        if(jsonEncode(statistic_known['words'][i]['known']) == 'true' ){
          knownWords += 1;
        }
        attempts += int.parse(jsonEncode(statistic_known['words'][i]['attempts']));
      };

      avgAns = (attempts/(statistic_known['words'].length)) ;

      // Map word1 = wordsList[0];

      // print(word1['id']);

    }
    catch (e) {
      print('caught error: $e');
      statistic_id = '0';
      user_id = '0';
      statistic_hits = '0';
      statistic_fails = '0';
      statistic_known = {"result": "error"};
    }


  }

}
