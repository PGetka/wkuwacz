import 'package:http/http.dart';
import 'dart:convert';

class Login {

  String user_login_test; // user login from app user
  String user_password_test; // user password from app user
  String user_login; 
  String user_id; 
  String user_native_language_id; 


  Login({ this.user_login_test, this.user_password_test});


  Future<void> checkData() async {

    try {

      //make the request
      Response response = await post('http://serwer1969421.home.pl/wkuwacz/login.php', body: {
      "username" : user_login_test.trim(),
      "password" : user_password_test.trim(),
      });
      List data = jsonDecode(response.body);
      Map dataMap = data[0];

      user_login = dataMap['user_login'];
      user_id = dataMap['user_id'];
      user_native_language_id = dataMap['user_native_language_id'];

    }
    catch (e) {
      print('caught error: $e');
      user_login = 'error';
    }


  }

}
