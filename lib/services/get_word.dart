import 'package:http/http.dart';
import 'dart:convert';
import 'dart:math';

class GetWord {

  int word_count;
  int word_id; 
  String word_content; 
  String word_language_id; 
  String apiKey = 'trnsl.1.1.20191215T231617Z.006a71fa7a87c44e.cc9c552d51b3845002f6fdf721738cbe34cb6fa6';
  String langFrom;
  String langTo;
  String translatedWord;
  

  GetWord({this.langTo});


   Future<void> getData() async {

    try {

      //make the request
      Response count_response = await post('http://serwer1969421.home.pl/wkuwacz/count_words.php', body: {
      });
      List count_data = jsonDecode(count_response.body);
      word_count = int.parse(count_data[0]['COUNT(*)']);
      var rng = new Random();
      word_id = rng.nextInt(word_count)+1;
      print(word_id);

      Response word_response = await post('http://serwer1969421.home.pl/wkuwacz/get_word.php', body: {
        "word_id" : word_id.toString()
      });
      List word_data = jsonDecode(word_response.body);
      print(word_data[0]);
      word_content = word_data[0]['word_content'];
      
      Response lang_response = await post('http://serwer1969421.home.pl/wkuwacz/get_language.php', body: {
        "word_language_id" : word_data[0]['word_language_id']
      });
      List lang_data = jsonDecode(lang_response.body);
      print(lang_data);
      langFrom = lang_data[0]['language_short_name'];

      Response trans_response = await post('https://translate.yandex.net/api/v1.5/tr.json/translate?key=${apiKey}&text=${word_content}&lang=${langFrom}-${langTo}&[format=plain]');
      Map trans_data = jsonDecode(trans_response.body);
      print(trans_data);
      translatedWord = trans_data['text'][0];
      print(translatedWord);


    }
    catch (e) {
      print('caught error: $e');
      word_content = 'error';
    }


  }

}
