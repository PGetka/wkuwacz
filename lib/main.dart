import 'package:flutter/material.dart';
import 'package:wkuwacz/pages/home.dart';
import 'package:wkuwacz/pages/choose_language.dart';
import 'package:wkuwacz/pages/learning.dart';
import 'package:wkuwacz/pages/login_screen.dart';
import 'package:wkuwacz/pages/statistics.dart';
import 'package:wkuwacz/pages/settings.dart';


void main() => runApp(MaterialApp(
  title: 'Wkuwacz',
  debugShowCheckedModeBanner: false,
  initialRoute: '/',
  routes: {
    '/': (context) => LoginScreen(),
    '/home': (context) => Home(),
    '/language': (context) => ChooseLanguage(),
    '/statistics': (context) => Statistics(),
    '/settings': (context) => Settings(),
    '/learning': (context) => Learning(),
  },
  )
);

