import 'package:flutter/material.dart';


class ChooseLanguage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Map data = ModalRoute.of(context).settings.arguments;
    print(data);
    
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Języki',
          style: TextStyle(
            color: Colors.grey[50],
          ),
          ),
        centerTitle: true,
        backgroundColor: Colors.green[600],
      ),
      body: SingleChildScrollView(
          child: Container(
          margin: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(height: 10.0),
              RaisedButton(
                onPressed: (){
                  Navigator.pushNamed(context, '/learning', arguments: {
                    'language_short' : 'en',
                    'language' : 'Angielski',
                    'user_native_language_id' : data['user_native_language_id'],
                    'user_id' : data['user_id']
                  } );
                },
                color: Colors.green[600],
                child: Text(
                  'Angielski',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.grey[50],
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              RaisedButton(
                onPressed: (){
                  Navigator.pushNamed(context, '/learning', arguments: {
                    'language_short' : 'de',
                    'language' : 'Niemiecki',
                    'user_native_language_id' : data['user_native_language_id'],
                    'user_id' : data['user_id']
                  } );
                },
                color: Colors.green[600],
                child: Text(
                  'Niemiecki',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.grey[50],
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              RaisedButton(
                onPressed: (){
                  Navigator.pushNamed(context, '/learning', arguments: {
                    'language_short' : 'ru',
                    'language' : 'Rosyjski',
                    'user_native_language_id' : data['user_native_language_id'],
                    'user_id' : data['user_id']
                  } );
                },
                color: Colors.green[600],
                child: Text(
                  'Rosyjski',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.grey[50],
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              RaisedButton(
                onPressed: (){
                  Navigator.pushNamed(context, '/learning', arguments: {
                    'language_short' : 'fr',
                    'language' : 'Francuski',
                    'user_native_language_id' : data['user_native_language_id'],
                    'user_id' : data['user_id']
                  } );
                },
                color: Colors.green[600],
                child: Text(
                  'Francuski',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.grey[50],
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              RaisedButton(
                onPressed: (){
                  Navigator.pushNamed(context, '/learning', arguments: {
                    'language_short' : 'it',
                    'language' : 'Włoski',
                    'user_native_language_id' : data['user_native_language_id'],
                    'user_id' : data['user_id']
                  } );
                },
                color: Colors.green[600],
                child: Text(
                  'Włoski',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.grey[50],
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              RaisedButton(
                onPressed: (){
                  Navigator.pushNamed(context, '/learning', arguments: {
                    'language_short' : 'pl',
                    'language' : 'Polski',
                    'user_native_language_id' : data['user_native_language_id'],
                    'user_id' : data['user_id']
                  } );
                },
                color: Colors.green[600],
                child: Text(
                  'Polski',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.grey[50],
                  ),
                ),
              ),
              SizedBox(height: 10.0),
            ],
          ),
        ),
      ),
    );
  }
}