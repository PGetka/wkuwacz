import 'package:flutter/material.dart';
import 'package:wkuwacz/services/get_statistic.dart';

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  Map data;

  Future<void> setupStatistic() async {
    GetStatistic instance = GetStatistic(user_id : data['user_id']);
    await instance.checkData();
    Navigator.pushNamed(context, '/statistics', arguments: {
      'user_id' : instance.user_id,
      'statistic_id' : instance.statistic_id,
      'statistic_hits' : instance.statistic_hits,
      'statistic_fails' : instance.statistic_fails,
      'statistic_known' : instance.statistic_known,
      'knownWords' : instance.knownWords,
      'avgAns' : instance.avgAns,
    });
  }


  @override
  Widget build(BuildContext context) {

  data = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Menu',
          style: TextStyle(
            color: Colors.grey[50],
          ),
          ),
        centerTitle: true,
        backgroundColor: Colors.green[600],
      ),

      body:Container(
        margin: EdgeInsets.fromLTRB(16.0, 90.0, 16.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            RaisedButton(
              onPressed: (){
                Navigator.pushNamed(context, '/language', arguments: {
                  'user_native_language_id' : data['user_native_language_id'],
                  'user_id' : data['user_id'],
                });
              },
              color: Colors.green[600],
              child: Text(
                'Języki',
                style: TextStyle(
                  fontSize: 25,
                  letterSpacing: 1,
                  color: Colors.grey[50],
                ),
              ),
            ),
            SizedBox(height: 10,),
            RaisedButton(
              onPressed: () {
                setupStatistic();
              },
              color: Colors.green[600],
              child: Text(
                'Statystyki',
                style: TextStyle(
                  fontSize: 25,
                  letterSpacing: 1,
                  color: Colors.grey[50],
                ),
              ),
            ),
            SizedBox(height: 10,),
            RaisedButton(
              color: Colors.green[600],
              onPressed: (){
                Navigator.pushNamed(context, '/settings', arguments: {
                  'user_id' : data['user_id']
                });
              },
              child: Text(
                'Ustawienia',
                style: TextStyle(
                  fontSize: 25,
                  letterSpacing: 1,
                  color: Colors.grey[50],
                ),
              ),
            )
          ],
        ),
      ),
      
    );
  }
}