import 'package:flutter/material.dart';
import 'package:wkuwacz/services/get_word.dart';
import 'package:wkuwacz/services/rebulid_stat.dart';

class Learning extends StatefulWidget {
  @override
  _LearningState createState() => _LearningState();
}

class _LearningState extends State<Learning> {

    Map data;
    int word_id;
    String translatedWord = '...';
    String word_content;
    bool flag = false;
    String done = '';
    String buttonMsg1 = 'Sprawdź';
    String buttonMsg2 = 'Podpowiedź';
    String answerMessage = '';
    Color answerMessageColor = Colors.green[600];

  Future<void> getWord() async {
    GetWord instance = GetWord(langTo : data['language_short']);
    await instance.getData();
    setState(() {
      translatedWord = instance.translatedWord.toString();
      word_content = instance.word_content.toString();
      flag = true;
      word_id = instance.word_id;
    });
    print(translatedWord);
    print(word_content);
  }

    Future<void> reStatistic() async {
      ReStatistic instance = ReStatistic(user_id : data['user_id'], word_id : word_id, done : done);
      await instance.sendData();
  }

  void checkAns(answer){
    answer = answer.trim();
    answer = answer.toLowerCase();
    if(done == ''){
      if( answer == word_content){
        setState((){
          done = 'true';
          buttonMsg1 = 'Dalej';
          answerMessage = "Dobrze!";
          answerMessageColor = Colors.green[600];
          reStatistic();
        });
      }
      else{
        setState(() {
          done = 'false';
          answerMessage = 'Źle!';
          buttonMsg2 = 'Odpowiedź';
          buttonMsg1 = 'Dalej';
          answerMessageColor = Colors.red;
          reStatistic();
        });
      }
    }
  }

  void hintOrAns(){
    if(done == ''){
      setState(() {
        answerMessage = '${word_content[0]}...${word_content[word_content.length-1]}';
      });
    }if(done == 'false'){
      setState(() {
        answerMessage = word_content;
      }); 
    }
  }

  void nextWord(){
    if(done != ''){
      Navigator.pushReplacementNamed(context, '/learning', arguments: {
        'language_short' : data['language_short'],
        'language' : data['language'],
        'user_native_language_id' : data['user_native_language_id'],
        'user_id' : data['user_id'],
        
      });
    }
  }


  @override
  Widget build(BuildContext context) {

    TextEditingController answer = new TextEditingController();
    data = ModalRoute.of(context).settings.arguments;
    if (flag == false){
      getWord();
    }

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.green[600],
        title: Text(
          data['language'],
          style: TextStyle(
            color: Colors.grey[50],
          )
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
          child: Container(
          margin: EdgeInsets.fromLTRB(16.0, 80.0, 16.0, 0.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Card(
                color: Colors.green[600],
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 30.0,),
                    Text(
                      translatedWord,
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.grey[50],
                      ),
                    ),
                    SizedBox(height: 10.0,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 50.0),
                      child: TextFormField(
                        controller: answer,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.grey[50],
                        ),
                        cursorColor: Colors.grey[50],
                        decoration: InputDecoration(
                          hintText: 'Wprowadź odpowiedź',
                          hintStyle: TextStyle(
                            color: Colors.green[300],
                          ),
                          fillColor: Colors.grey[50],
                          labelStyle: TextStyle(
                            color: Colors.grey[50],
                            fontSize: 18.0,
                            letterSpacing: 0.8,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey[50],
                            )
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey[50],
                            )
                          )
                        ),
                      ),
                    ),
                    SizedBox(height: 30.0,),
                  ],
                ),
              ),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  RaisedButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 38.0),
                      child: Text(
                        buttonMsg2,
                        style: TextStyle(
                          color: Colors.grey[50],
                        )
                      ),
                    ),
                    onPressed: (){
                      hintOrAns();
                    },
                    color: Colors.green[600],
                  ),
                  SizedBox(width: 10,),
                  RaisedButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 39.0),
                      child: Text(
                        buttonMsg1,
                        style: TextStyle(
                          color: Colors.grey[50],
                        )
                      ),
                    ),
                    onPressed: (){
                      nextWord();
                      checkAns(answer.text);
                    },
                    color: Colors.green[600],
                  )
                ],
              ),
              RaisedButton(
                child:Text(
                  'Zakończ',
                  style: TextStyle(
                    color: Colors.grey[50],
                  ),
                ),
                onPressed: (){
                  Navigator.pop(context);
                },
                color: Colors.green[600],
              
              ),
              SizedBox(height: 60,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Center(
                  child: Text(
                    answerMessage,
                    style: TextStyle(
                      fontSize: 20.0,
                      color: answerMessageColor,
                    ),
                  ),
                ),
              ),
              )
            ],
          ),
        ),
      ),
    );
  }
}