import 'package:flutter/material.dart';


class Statistics extends StatefulWidget {
  @override
  _StatisticsState createState() => _StatisticsState();
}

class _StatisticsState extends State<Statistics> {


  @override
  Widget build(BuildContext context) {

    Map data = ModalRoute.of(context).settings.arguments;

    num hitsToFails = ((int.parse(data['statistic_hits']))/(int.parse(data['statistic_fails'])));

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[600],
        title: Text(
          'Statystyki',
          style: TextStyle(
            color: Colors.grey[50]
            ),
        ),
        centerTitle: true,
      ),
      body:SingleChildScrollView(
          child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Card(
                margin: EdgeInsetsDirectional.fromSTEB(16, 16, 16, 0),
                color: Colors.green[600],
                child: Padding(

                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(

                      'Opanowane słowa:',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.grey[50],
                      ),
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsetsDirectional.fromSTEB(16, 0, 16, 0),
                color: Colors.green[300],
                child: Padding(

                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(
                      '${data['knownWords']}',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.grey[50],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0,),
              Card(
                margin: EdgeInsetsDirectional.fromSTEB(16, 16, 16, 0),
                color: Colors.green[600],
                child: Padding(

                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(

                      'Średnia ilość powtórzeń:',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.grey[50],
                      ),
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsetsDirectional.fromSTEB(16, 0, 16, 0),
                color: Colors.green[300],
                child: Padding(

                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(
                      '${data['avgAns']}',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.grey[50],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0,),
              Card(
                margin: EdgeInsetsDirectional.fromSTEB(16, 16, 16, 0),
                color: Colors.green[600],
                child: Padding(

                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(

                      'Ilość błędnych odpowiedzi:',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.grey[50],
                      ),
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsetsDirectional.fromSTEB(16, 0, 16, 0),
                color: Colors.green[300],
                child: Padding(

                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(

                      data['statistic_fails'],
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.grey[50],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0,),
              Card(
                margin: EdgeInsetsDirectional.fromSTEB(16, 16, 16, 0),
                color: Colors.green[600],
                child: Padding(

                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(

                      'Ilość poprawnych odpowiedzi:',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.grey[50],
                      ),
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsetsDirectional.fromSTEB(16, 0, 16, 0),
                color: Colors.green[300],
                child: Padding(

                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(

                      data['statistic_hits'],
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.grey[50],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0,),
              Card(
                margin: EdgeInsetsDirectional.fromSTEB(16, 16, 16, 0),
                color: Colors.green[600],
                child: Padding(

                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(

                      'Poprawne do błędnych:',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.grey[50],
                      ),
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsetsDirectional.fromSTEB(16, 0, 16, 0),
                color: Colors.green[300],
                child: Padding(

                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(
                      '$hitsToFails',
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.grey[50],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16.0,)
              
            ],
          ),
        ),
      ),
    );
  }
}