import 'package:flutter/material.dart';
import 'package:wkuwacz/services/login.dart';

class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  TextEditingController user = new TextEditingController();
  TextEditingController pass = new TextEditingController();

  String login_msg = '';



  void setupLogin() async {
    Login instance = Login(user_login_test: user.text, user_password_test: pass.text);
    await instance.checkData();
    if(instance.user_login == 'error'){
      setState(() {
        login_msg = 'Błędny login lub hasło!';
      });
      
    }
    else{
      Navigator.pushReplacementNamed(context, '/home', arguments: {
        'user_id' : instance.user_id,
        'user_login': instance.user_login,
        'user_native_language_id': instance.user_native_language_id,
    });
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Logowanie',
          style: TextStyle(
            color: Colors.grey[50],
          ),

        ),
        centerTitle: true,
        backgroundColor: Colors.green[600],
      ),
      body:SingleChildScrollView(
              child: Container(
          child: Center(
            child:Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 90.0,
                  ),
                  Text(
                    '$login_msg',
                    style: TextStyle(
                      fontSize: 20.0,
                      letterSpacing: 0.8,
                      color: Colors.red,
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: user,
                    decoration: InputDecoration(
                      labelText: 'wprowadź login',
                      labelStyle: TextStyle(
                        color: Colors.green[600],
                        fontSize: 18.0,
                        letterSpacing: 0.8,
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.green[600],
                        )
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.green[600],
                        )
                      )
                    ),
                  ),
                  TextFormField(
                    controller: pass,
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: 'wprowadź hasło',
                      labelStyle: TextStyle(
                        color: Colors.green[600],
                        fontSize: 18.0,
                        letterSpacing: 0.8,
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.green[600],
                        )
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.green[600],
                        )
                      )
                    ),
                    
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  RaisedButton(
                    padding: EdgeInsets.symmetric(horizontal: 50),
                    color: Colors.green[400],
                    onPressed: () async {
                      setupLogin();
                    },
                    child: Text(
                      'zaloguj',
                      style: TextStyle(
                        color: Colors.grey[50],
                        fontSize: 18.0
                      ),
                      
                    ),
                  ),
                  SizedBox(height: 100,)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}