import 'package:flutter/material.dart';
import 'package:wkuwacz/services/clear_statistic.dart';

class Settings extends StatelessWidget {

  Map data;

  Future<void> clearStatistic() async {
    ClearStatistic instance = ClearStatistic(user_id : data['user_id']);
    await instance.clearData();
  }


  @override
  Widget build(BuildContext context) {

    data = ModalRoute.of(context).settings.arguments;
    print(data);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Ustawienia',
          style: TextStyle(
            color: Colors.grey[50],
          ),
          ),
        centerTitle: true,
        backgroundColor: Colors.green[600],
      ),

      body:Container(
        margin: EdgeInsets.fromLTRB(16.0, 90.0, 16.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            RaisedButton(
              onPressed: (){
                clearStatistic();
              },
              color: Colors.green[600],
              child: Text(
                'Wyczyść postępy',
                style: TextStyle(
                  fontSize: 25,
                  letterSpacing: 1,
                  color: Colors.grey[50],
                ),
              ),
            ),
            SizedBox(height: 10,),
            RaisedButton(
              onPressed: (){
                clearStatistic();
              },
              color: Colors.green[600],
              child: Text(
                'Oceń nas',
                style: TextStyle(
                  fontSize: 25,
                  letterSpacing: 1,
                  color: Colors.grey[50],
                ),
              ),
            ),
            SizedBox(height: 10,),
            RaisedButton(
              color: Colors.green[600],
              onPressed: (){

              },
              child: Text(
                'Zgłoś błędy',
                style: TextStyle(
                  fontSize: 25,
                  letterSpacing: 1,
                  color: Colors.grey[50],
                ),
              ),
            ),
          ],
        ),
      ),
      
    );
  }
}